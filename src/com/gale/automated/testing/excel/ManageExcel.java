package com.gale.automated.testing.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gable.automated.testing.constant.ManageConstant;

public class ManageExcel {

	XSSFWorkbook xwb = new XSSFWorkbook();
	FileOutputStream fos = null;
	String xldata[][];

	public String[][] xlRead(String sPath, String sheetName) {

		try {
			int xRows, xCols;
			File myxl = new File(sPath);
			FileInputStream myStream = new FileInputStream(myxl);
			@SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook(myStream);
			XSSFSheet sheet = null;

			sheet = wb.getSheet(sheetName);
			xRows = (sheet.getLastRowNum() + 1);
			xCols = sheet.getRow(0).getLastCellNum();
			xldata = new String[xRows][xCols];

			for (int i = 0; i < xRows; i++) {
				XSSFRow row = sheet.getRow(i);
				for (int j = 0; j < xCols; j++) {

					XSSFCell cell = row.getCell(j);

					String value = cellToString(cell);

					xldata[i][j] = value;

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			xldata = new String[1][1];
			xldata[0][0] = "0";
		}

		return xldata;

	}

	public String cellToString(XSSFCell cell) {
		try {

			CellType type = cell.getCellTypeEnum();
			Object result;
			switch (type) {
			case NUMERIC:
				result = cell.getNumericCellValue();
				break;
			case STRING:
				result = cell.getStringCellValue();
				break;
			case FORMULA:
				throw new RuntimeException("We can't evaluate formulas in Java");
			case BLANK:
				result = "";
				break;
			case BOOLEAN:
				result = cell.getBooleanCellValue();
				break;
			case ERROR:
				throw new RuntimeException("This cell has an error");
			default:
				throw new RuntimeException("We don't support this cell type: " + type);
			}

			return result.toString();

		} catch (Exception e) {
			return "--------";
		}
	}

	public void xlwrite_testCase(String[][] xldata) throws IOException {

		FileInputStream fileInputStream = new FileInputStream(ManageConstant.XLPATH_READ_TESTCASE);

		@SuppressWarnings("resource")
		XSSFWorkbook read_wbs = new XSSFWorkbook(fileInputStream);
		XSSFSheet read_sheets = read_wbs.getSheet("TestCases");

		XSSFSheet write_sheet = xwb.createSheet("TestCase_Output");

		for (int i = 0; i < xldata.length; i++) {

			XSSFRow row = read_sheets.getRow(i);
			XSSFRow write_row = write_sheet.createRow(i);

			for (int j = 0; j < read_sheets.getRow(i).getLastCellNum(); j++) {

				XSSFCell write_cell = write_row.createCell(j);
				XSSFCell cell = row.getCell(j);

				XSSFCellStyle cellStyle = cell.getCellStyle();
				XSSFCellStyle style = xwb.createCellStyle();

				write_sheet.setColumnWidth(write_cell.getColumnIndex(),
						read_sheets.getColumnWidth(cell.getColumnIndex()));

				if (i == 0) {
					XSSFFont font = xwb.createFont();
					font.setColor(IndexedColors.WHITE.getIndex());
					style.setFont(font);
				}

				style.setFillForegroundColor(cellStyle.getFillForegroundColorColor());
				style.setFillPattern(cellStyle.getFillPatternEnum());
				style.setBorderTop(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				style.setBorderBottom(BorderStyle.THIN);
				style.setBorderLeft(BorderStyle.THIN);
				write_cell.setCellType(CellType.STRING);
				write_cell.setCellStyle(style);
				write_cell.setCellValue(xldata[i][j]);

			}
		}

		if (fos == null) {
			fos = new FileOutputStream(ManageConstant.XLPATH_WRITE_TESTRESULT_OUTPUT);
		}

	}

	public void xlwrite_testData(String[][] xldata, String nameTestCase) throws Exception {

		FileInputStream fileInputStream = new FileInputStream(ManageConstant.XLPATH_READ_TESTCASE);

		@SuppressWarnings("resource")
		XSSFWorkbook read_wbs = new XSSFWorkbook(fileInputStream);
		XSSFSheet read_sheets = read_wbs.getSheet(nameTestCase);

		XSSFSheet write_sheet = xwb.createSheet("" + nameTestCase);

		for (int i = 0; i < xldata.length; i++) {

			XSSFRow row = read_sheets.getRow(i);
			XSSFRow write_row = write_sheet.createRow(i);

			for (int j = 0; j < read_sheets.getRow(i).getLastCellNum(); j++) {

				XSSFCell write_cell = write_row.createCell(j);
				XSSFCell cell = row.getCell(j);

				XSSFCellStyle cellStyle = cell.getCellStyle();
				XSSFCellStyle style = xwb.createCellStyle();

				write_sheet.setColumnWidth(write_cell.getColumnIndex(),
						read_sheets.getColumnWidth(cell.getColumnIndex()));

				if (i == 0) {
					XSSFFont font = xwb.createFont();
					font.setColor(IndexedColors.WHITE.getIndex());
					style.setFont(font);
				}

				style.setFillForegroundColor(cellStyle.getFillForegroundColorColor());
				style.setFillPattern(cellStyle.getFillPatternEnum());
				style.setBorderTop(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				style.setBorderBottom(BorderStyle.THIN);
				style.setBorderLeft(BorderStyle.THIN);
				write_cell.setCellType(CellType.STRING);
				write_cell.setCellStyle(style);
				write_cell.setCellValue(xldata[i][j]);

			}
		}

	}
	
	public void tests_xlwrite_testStep(String[][] xldata  , int lastNumberColumn) throws Exception {
		
		FileInputStream fileInputStream = new FileInputStream(ManageConstant.XLPATH_READ_TESTCASE);
		
		@SuppressWarnings("resource")
		XSSFWorkbook read_wbs = new XSSFWorkbook(fileInputStream);
		XSSFSheet read_sheets = read_wbs.getSheet("TestSteps");
		
		XSSFSheet write_sheet = xwb.createSheet("TestStep_Output");

		for (int i = 0; i < xldata.length ; i++) {
			
			XSSFRow row = read_sheets.getRow(i);
			XSSFRow write_row = write_sheet.createRow(i);

			for (int j = 0; j < (lastNumberColumn+1) ; j++) {

				XSSFCell write_cell = write_row.createCell(j);
				XSSFCell cell = null;
				
				if(j < read_sheets.getRow(i).getLastCellNum()){
					cell = row.getCell(j);
				}else{
					cell = (row.getCell(read_sheets.getRow(i).getLastCellNum()-2));
				}
				
				XSSFCellStyle cellStyle = cell.getCellStyle();
				XSSFCellStyle style = xwb.createCellStyle();
				
				write_sheet.setColumnWidth(write_cell.getColumnIndex(), read_sheets.getColumnWidth(cell.getColumnIndex()));

				if(i == 0){
					 XSSFFont font = xwb.createFont();
				     font.setColor(IndexedColors.WHITE.getIndex());
				     style.setFont(font);
				}
				
				style.setFillForegroundColor(cellStyle.getFillForegroundColorColor());
				style.setFillPattern(cellStyle.getFillPatternEnum());
				style.setBorderTop(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				style.setBorderBottom(BorderStyle.THIN);
				style.setBorderLeft(BorderStyle.THIN);
				
				write_cell.setCellType(CellType.STRING);
				write_cell.setCellStyle(style);
				write_cell.setCellValue(xldata[i][j]);

			}
		}
	}
	public void finishWrite_TestResult() {
		try {
			xwb.write(fos);
			xwb.close();
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

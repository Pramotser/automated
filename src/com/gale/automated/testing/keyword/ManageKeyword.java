package com.gale.automated.testing.keyword;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gable.automated.testing.report.ExtentReport;

public class ManageKeyword {
	static String check = "";
	static String error = "";

	WebDriver driver = null;

	public static String REQ_NO = null;
	public static String BOX_NO = null;
	public static String ENV_NAME = null;
	public static String CLIPBOARD = null;
	public static String USERNAME = null;
	public static String textSpan = null;

	public ManageKeyword(WebDriver webdriver) {
		this.driver = webdriver;
	}

	public ManageKeyword() {
	}

	public String keyword_executor(String keyword, String xpath, String sendkey) throws Exception {
		check = "fail";
		try {
			if (keyword.equals("browser_open")) {
				return browser_open(driver, sendkey);
			} else if (keyword.equals("click_link")) {
				return cilck_link(driver, xpath);
			} else if (keyword.equals("edit_input")) {
				return edit_input(driver, xpath, sendkey);
			} else if (keyword.equals("edit_action")) {
				return edit_action(driver, xpath, sendkey);
			} else if (keyword.equals("click_right")) {
				return click_right(driver, xpath);
			} else if (keyword.equals("switch_iframe")) {
				return switch_iframe(driver);
			} else if (keyword.equals("clear_iframe")) {
				return clear_iframe(driver);
			} else if (keyword.equals("switch_popup")) {
				return switch_popup(driver, xpath);
			} else if (keyword.equals("double_click")) {
				return double_click(driver, xpath);
			} else if (keyword.equals("upload_file")) {
				return upload_file(driver, xpath, sendkey);
			} else if (keyword.equals("radio_select")) {
				return radio_select(driver, xpath, sendkey);
			} else if (keyword.equals("button_click")) {
				return button_click(driver, xpath);
			} else if (keyword.equals("display")) {
				return display(driver, xpath, sendkey);
			} else if (keyword.equals("browser_close")) {
				return browser_close(driver);
			} else if (keyword.equals("list_select")) {
				return list_select(driver, xpath, sendkey);
			} else if (keyword.equals("checkbox_set")) {
				return checkbox_set(driver, xpath, sendkey);
			} else if (keyword.equals("myWait")) {
				return myWait(driver, xpath);
			} else if (keyword.equals("list_select2")) {
				return list_select2(driver, xpath);
			} else if (keyword.equals("mouse_click")) {
				return mouse_click(driver, xpath);
			} else if (keyword.equals("scroll_top")) {
				return scroll_top(driver, xpath);
			} else if (keyword.equals("remember")) {
				return remember(driver, xpath, sendkey);
			} else if (keyword.equals("click_table_req_no")) {
				return click_table_req_no(driver);
			} else if (keyword.equals("generate_barcode")) {
				return generate_barcode(driver, xpath);
			} else if (keyword.equals("list_select_row")) {
				return list_select_row(driver, xpath);
			} else if (keyword.equals("key_enter")) {
				return key_enter(driver, xpath);
			} else if (keyword.equals("dialog_click")) {
				return dialog_click(driver);
			} else if (keyword.equals("text_click")) {
				return text_click(driver, xpath);
			} else if (keyword.equals("crop_move")) {
				return crop_move(driver, xpath);
			} else if (keyword.equals("stopWatch")){
				return stopWatch(driver);
			} else if (keyword.equals("loop_select")){
				return loop_select(driver, xpath);
			} else if (keyword.equals("url_open")) {
				return url_open(driver, xpath);
			} else if (keyword.equals("event")) {
				return event(xpath);
			} else if (keyword.equals("download_report")) {
				return download_report(driver);
			} else {
				return "invalid Keyword";
			}
		} catch (Exception e) {
			setError("Error Check Keyword");

			return error;
		}

	}

	public static String browser_open(WebDriver webDriver, String sendkey) {
		try {
			webDriver.navigate().to(sendkey);
//			webDriver.navigate().to("javascript:document.getElementById('overridelink').click()");
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "browser_open";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String cilck_link(WebDriver webDriver, String locator) {
		try {
			Thread.sleep(2000);
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			WebElement elem = webDriver.findElement(By.xpath(locator));
			js.executeScript("arguments[0].click();", elem);
			Thread.sleep(2000);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "cilck_link";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String edit_input(WebDriver webDriver, String locator, String sendkey) {

		myWait(webDriver, locator);
		try {
			WebElement we = webDriver.findElement(By.xpath(locator));
			we.clear();
			// System.out.println("keyword ::" + sendkey);
			switch (sendkey) {
			case "CLIPBOARD":
				we.sendKeys(ManageKeyword.CLIPBOARD);
				System.out.println("CLIPBOARD >>>>>>>>>>>>>>>> " + ManageKeyword.CLIPBOARD);
				break;
			case "ENV_NAME":
				we.sendKeys(ManageKeyword.ENV_NAME);
				break;
			case "BOX_NO":
				we.sendKeys(ManageKeyword.BOX_NO);
				break;
			case "REQ_NO":
//				REQ_NO = "101090";
				we.sendKeys(ManageKeyword.REQ_NO);
				break;
			case "USERNAME":
//				CLIPBOARD = "512526";
			    we.sendKeys("K0" + ManageKeyword.CLIPBOARD);
			    break;
			default:
				we.sendKeys(sendkey);
				break;
			}
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "edit_input";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String edit_action(WebDriver webDriver, String locator, String sendkey) {
		myWait(webDriver, locator);
		try {
			Actions action = new Actions(webDriver);
			System.out.println(">>>>>>>>" + sendkey);
			action.sendKeys(sendkey).sendKeys(Keys.ENTER).perform();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "edit_action";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String key_enter(WebDriver webDriver, String locator) {
		myWait(webDriver, locator);
		try {
			webDriver.findElement(By.xpath(locator)).sendKeys(Keys.ENTER);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "key_enter";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String click_right(WebDriver webDriver, String locator) {
		myWait(webDriver, locator);
		try {
			Actions action = new Actions(webDriver);
			action.contextClick(webDriver.findElement(By.xpath(locator))).build().perform();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "click_right";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String switch_iframe(WebDriver webDriver) {

		try {
			List<WebElement> iframe = webDriver.findElements(By.tagName("iframe"));
			System.out.println("size :: " + iframe.size());
			webDriver.switchTo().frame(0);
			if (iframe.size() < 1) {
				check = "fail";
				error = "invalid size switch_iframe";
			} else {
				check = "pass";
			}
		} catch (Exception ex) {
			check = "fail";
			error = "switch_iframe";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String clear_iframe(WebDriver webDriver) {
		try {
			webDriver.switchTo().defaultContent();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "clear_iframe";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String switch_popup(WebDriver webDriver, String locator) {
		try {
			Thread.sleep(5000);
			Set<String> windows = webDriver.getWindowHandles();
			String xx = null;
			for (String string : windows) {
				xx = string;
				webDriver.switchTo().window(xx);
				System.out.println(webDriver.getCurrentUrl());

				if (webDriver.getCurrentUrl().contains(locator)) {
					if (!locator.equals(webDriver.getCurrentUrl())) {
						System.out.println("Url1 >> " + webDriver.getCurrentUrl());
						System.out.println("Yess");
						webDriver.switchTo().window(xx);
						check = "pass";
						error = "";

					} else {
						System.out.println(locator + "<<< Url >>> " + webDriver.getCurrentUrl());
						webDriver.switchTo().window(xx);
						check = "pass";
						error = "";
						break;
					}

					//
				} else {
					check = "fail";
					error = "invalid_Url_switch_popup";
					System.out.println("Url2 >> " + webDriver.getCurrentUrl());
				}
			}
			System.out.println("Xpath >>> " + locator);
		} catch (Exception ex) {
			check = "fail";
			error = "switch_popup";
			System.out.println(error + ex.getMessage());
		}
		return check;
	}

	public static String crop_move(WebDriver webDriver, String locator) {
		// myWait(webDriver, locator);
		try {
			WebElement element = webDriver.findElement(By.xpath(locator));
			Actions builder = new Actions(webDriver);
			
			
//			int offsetX = element.getLocation().x + 0 - element.getLocation().x;
//		    int offsetY = element.getLocation().y + 100 - element.getLocation().y;
			Point point = element.getLocation();
//			builder.dragAndDropBy(element, 0,0).perform();
			builder.clickAndHold(element).moveByOffset(0, 1).release().build().perform();
			builder.clickAndHold(element).moveByOffset(10, 100).release().build().perform();
			builder.clickAndHold(element).moveByOffset(400, 150).release().build().perform();
			int xcord = point.getX();
			int ycord = point.getY();
			System.out.println("w:" + element.getSize().width + " h:" + element.getSize().height + ",,,,,,,,,,,,,, " + xcord + ",,," + ycord);
			System.out.println("Pass >>><<<<<<<<<<<<<<<<<<<<<<<");
			check = "pass";
			error = "";
		} catch (Exception ex) {
			check = "fail";
			error = "crop invalied";
			System.err.println(ex);
		}
		return check;
	}
	
	public static String crop_sig(WebDriver webDriver, String locator) {
		// myWait(webDriver, locator);
		try {
			WebElement element = webDriver.findElement(By.xpath(locator));
			Actions builder = new Actions(webDriver);
			builder.clickAndHold(element).moveByOffset(0, 0).release().build().perform();
			System.out.println("Pass >>><<<<<<<<<<<<<<<<<<<<<<<");
			
			check = "pass";
			error = "";
		} catch (Exception ex) {
			check = "fail";
			error = "crop invalied";
			System.err.println(ex);
		}
		return check;
	}

	public static String double_click(WebDriver webDriver, String locator) {
		try {
			Actions action = new Actions(webDriver);
			WebElement element = webDriver.findElement(By.xpath(locator));
			// Double click
			action.doubleClick(element).perform();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "double_click";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String upload_file(WebDriver webDriver, String locator, String sendkey) {

		// myWait(webDriver, locator);
		try {

			Thread.sleep(1000);
			webDriver.findElement(By.xpath(locator)).sendKeys(sendkey);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "upload_file";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String radio_select(WebDriver webDriver, String locator, String sendkey) {

		// myWait(webDriver, locator);
		WebElement radioCheck = null;
		try {
			List<WebElement> radio = webDriver.findElements(By.xpath(locator));
			JavascriptExecutor js = (JavascriptExecutor) webDriver;

			for (int i = 0; i < radio.size(); i++) {
				System.out.println("vSendkey " + sendkey + " :::::::::::::" + radio.get(i).getAttribute("value"));
				String value = radio.get(i).getAttribute("value").toString();
				if (value.equals(sendkey)) {
					// radioCheck = radio.get(i);

					WebElement elem = radio.get(i);
					js.executeScript("arguments[0].click();", elem);
					// radioCheck.click();
					check = "pass";
				} /*
					 * else { check = "fail"; error =
					 * "invalid sendkey radio_select"; }
					 */
			}
			Thread.sleep(1000);
		} catch (Exception ex) {
			check = "fail";
			error = "radio_select";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String button_click(WebDriver webDriver, String locator) {

		myWait(webDriver, locator);
		try {
			Thread.sleep(1000);
			WebElement we = webDriver.findElement(By.xpath(locator));
			UtiliyScrollIntoView.moveTo(webDriver, we);
			we.click();
			Thread.sleep(1000);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "button_click";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String display(WebDriver webDriver, String locator, String sendkey) {

		myWait(webDriver, locator);
		try {

			WebElement we = webDriver.findElement(By.xpath(locator));
			String w = null;
			System.out.println(sendkey);
			if (sendkey.equalsIgnoreCase("text")) {
				w = we.getText().trim();
				System.out.println("display get text : " + w);
				if(!w.isEmpty()){
					check = "pass";
				}
				
			} else if (sendkey.equalsIgnoreCase("value")) {
				w = we.getAttribute("value").trim();
				System.out.println("display get value : " + w);
				if(!w.isEmpty()){
					check = "pass";
				}
			} else {
				check = "fail";
				error = "Invaild get display";
			}

		} catch (Exception ex) {
			check = "fail";
			error = "display";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String browser_close(WebDriver webDriver) {

		try {

			Set<String> windows = webDriver.getWindowHandles();
			for (String string : windows) {
				webDriver.switchTo().window(string);
				webDriver.close();
			}
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "browser_close";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String errorMsg(WebDriver webDriver, String locator, String expected) {

		myWait(webDriver, locator);
		try {

			WebElement we = webDriver.findElement(By.xpath(locator));
			textSpan = we.getText().trim();
			System.out.println("Actual Result :: " + textSpan + " == Expected Result ::" + expected);

			if (expected.equals(textSpan)) {

				check = "pass";
				error = "";
			} else {
				check = "fail";
				error = "Actual Result :: " + textSpan + " != Expected Result ::" + expected;
				System.out.println("Actual Result :: " + textSpan + " != Expected Result ::" + expected);
			}

		} catch (Exception e) {
			check = "fail";
			error = "Invalid errorMsg : " + e.getMessage();
			System.out.println(error);

		}

		return check;

	}

	public static String dialog_click(WebDriver webDriver) {

		try {
			Alert alert = webDriver.switchTo().alert();

			alert.accept();
			Thread.sleep(2000);
			check = "pass";

		} catch (Exception ex) {
			check = "fail";
			error = "Invalid dialog_click";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String dialog_accept(WebDriver webDriver, String expecResult) {

		try {
			Alert alert = webDriver.switchTo().alert();

			textSpan = alert.getText().trim();
			if (textSpan.equals(expecResult)) {
				System.out.println("Actual Result :: " + textSpan + " == Expected Result ::" + expecResult);
				alert.accept();
				Thread.sleep(2000);
				check = "pass";
				error = "";
			} else {

				System.out.println("Actual Result :: " + textSpan + " != Expected Result ::" + expecResult);
				check = "fail";
				error = "Actual Result :: " + textSpan + " == Expected Result ::" + expecResult;
			}

		} catch (Exception ex) {
			check = "fail";
			error = "Invalid dialog_accept";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String dialog_dismiss(WebDriver webDriver, String expecResult) {

		try {
			Alert alert = webDriver.switchTo().alert();

			textSpan = alert.getText().trim();
			if (textSpan.equals(expecResult)) {
				System.out.println("Actual Result :: " + textSpan + " == Expected Result ::" + expecResult);
				alert.dismiss();
				Thread.sleep(2000);
				check = "pass";
				error = "";
			} else {

				System.out.println("Actual Result :: " + textSpan + " != Expected Result ::" + expecResult);
				check = "fail";
				error = "Actual Result :: " + textSpan + " == Expected Result ::" + expecResult;
			}

		} catch (Exception ex) {
			check = "fail";
			error = "Invalid dialog_dismiss";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String list_select(WebDriver webDriver, String locator, String sendkey) {

		myWait(webDriver, locator);
		try {
			Thread.sleep(1000);
			WebElement we = webDriver.findElement(By.xpath(locator));
			Select objSelect = new Select(we);
			System.out.println("key ::: " + sendkey);
			try {
				if (sendkey.isEmpty()) {
					check = "pass";
				} else {
					objSelect.selectByVisibleText(sendkey);
					check = "pass";
				}

			} catch (Exception e) {
				check = "fail";
				error = "Invalid Sendkey list_select";
			}

		} catch (Exception ex) {
			check = "fail";
			error = "list_select";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String checkbox_set(WebDriver webDriver, String locator, String sendkey) {

		myWait(webDriver, locator);
		try {
			List<WebElement> checkBox = webDriver.findElements(By.xpath(locator));
			System.out.println("vSendkey :::::::::::::" + sendkey);
			String[] extract = sendkey.split(",");
			if (extract.length != 0) {
				for (int i = 0; i < extract.length; i++) {

					if (extract[i].equalsIgnoreCase("ON")) {
						System.out.println("extract :::::::::::::" + extract[i]);
						checkBox.get(i).click();
						check = "pass";
					} else if (extract[i].equalsIgnoreCase("OFF")) {
						check = "pass";
					} else {
						check = "fail";
						error = "Invalid CheckBox extract";
					}
				}

			} else {
				check = "fail";
				error = "Invalid CheckBox Selected";
			}
		} catch (Exception ex) {
			check = "fail";
			error = "checkbox_set";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String myWait(WebDriver webDriver, String locator) {

		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "myWait";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String list_select2(WebDriver webDriver, String locator) {

		myWait(webDriver, locator);
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(webDriver, 10);
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			UtiliyScrollIntoView.moveTo(webDriver, element);
			element.click();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "list_select2";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String mouse_click(WebDriver webDriver, String locator) {

		try {
			myWait(webDriver, locator);
			Actions act = new Actions(webDriver);
			WebElement elem = webDriver.findElement(By.xpath(locator));
			act.moveToElement(elem).perform();
			act.click().perform();
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "mouse_click";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String scroll_top(WebDriver webDriver, String locator) {

		try {

			JavascriptExecutor jse = (JavascriptExecutor) webDriver;
			jse.executeScript("$('html,body').scrollTop(0)");
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "mouse_click";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String remember(WebDriver webDriver, String locator, String sendkey) {

		try {

			myWait(webDriver, locator);
			Thread.sleep(3000);
			WebDriverWait wait = new WebDriverWait(webDriver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			String value = webDriver.findElement(By.xpath(locator)).getAttribute("value");
			System.out.println("Value2 " + value);
			switch (sendkey) {
			case "REQ_NO":
				System.out.println("Value " + value);
				ManageKeyword.REQ_NO = value;
				check = "pass";
				break;
			case "ENV_NAME":
				ManageKeyword.ENV_NAME = value;
				System.out.println("Value " + value);
				check = "pass";
				break;
			case "BOX_NO":

				ManageKeyword.BOX_NO = value;
				System.out.println("Value " + value);
				check = "pass";
				break;
			case "CLIPBOARD":
				Thread.sleep(2000);
				WebElement we = webDriver.findElement(By.xpath(locator));
				System.out.println("Value " + we.getText());
				ManageKeyword.CLIPBOARD = we.getText();
				System.out.println("CLIPBOARD of value :  :::: " + ManageKeyword.CLIPBOARD);
				check = "pass";
				break;
			}
			System.out.println(sendkey + ": " + value);
		} catch (Exception ex) {
			check = "fail";
			error = "remember";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String click_table_req_no(WebDriver webDriver) {

		// ManageKeyword.REQ_NO = "WD20180017401";
		try {
			Thread.sleep(5000);
			System.out.println(" " + ManageKeyword.REQ_NO);
			String xPath = "//td[text()='" + ManageKeyword.REQ_NO + "']";
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			WebElement elem = webDriver.findElement(By.xpath(xPath));
			js.executeScript("arguments[0].click();", elem);
			Thread.sleep(2000);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "click_table_req_no";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String generate_barcode(WebDriver webDriver, String locator) {

		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(webDriver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			List<WebElement> elems = webDriver.findElements(By.xpath(locator));
			Calendar cal;
			long time;

			System.out.println("GENERATED BARCODES: ");
			for (int i = 0; i < elems.size(); i++) {
				cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
				time = cal.getTimeInMillis();
				elems.get(i).sendKeys("AUTO" + time);
				System.out.println("AUTO" + time);
			}
			System.out.println("(TOTAL: " + elems.size() + ")");
			Thread.sleep(1000);
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "generate_barcode";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String list_select_row(WebDriver webDriver, String locator) {

		myWait(webDriver, locator);
		try {
			Thread.sleep(2000);
			String row = "100";

			WebElement we = webDriver.findElement(By.xpath(locator));
			Select objSelect = new Select(we);
			try {

				objSelect.selectByVisibleText(row);

				check = "pass";

				myWait(webDriver, locator);
			} catch (Exception e) {
				check = "fail";
				error = "Invalid Sendkey Row";
			}
		} catch (Exception ex) {
			check = "fail";
			error = "list_select_row";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public String[] checkExpectedResult(String dialog, String expecResult, String usecaseName, String TDID) {

		String results[] = new String[2];

		String[] temp = dialog.split(",");
		// try {
		if (temp[0].equalsIgnoreCase("Y")) {
			myWait(driver, temp[1]);
			try {
				if (temp[1].equalsIgnoreCase("dialog_accept")) {
					results[0] = ManageKeyword.dialog_accept(driver, expecResult);
					results[1] = ManageKeyword.getError();
				} else if (temp[1].equalsIgnoreCase("dialog_dismiss")) {
					results[0] = ManageKeyword.dialog_dismiss(driver, expecResult);
					results[1] = ManageKeyword.getError();
				} else {
					results[0] = ManageKeyword.errorMsg(driver, temp[1], expecResult);
					results[1] = ManageKeyword.getError();
				}
			} catch (Exception e) {
				results[0] = "Fail";
				results[1] = "Invalid Dialog Expected Result";
				System.out.println(" Fail Dialog ���++++++++++!!!!!!!!!!!!!");
			}
		} else if (temp[0].equalsIgnoreCase("N")) {
			try {
				// Thread.sleep(3000);

				System.out.println("ExResult =  " + expecResult + " AcResult = " + driver.getTitle() + " !!!");
				if (driver.getTitle().equals(expecResult)) {
					results[0] = "Pass";
					results[1] = "";
					// results[1] = expecResult;
				} else {
					results[0] = "Fail";
					results[1] = "ExResult:  " + expecResult + "UnEquals AcResult: " + driver.getTitle() + " !!!";
				}
			} catch (Exception e) {
				results[0] = "Fail";
				results[1] = "Invalid Title Expected Result";
				System.out.println(" Fail Title ���++++++++++!!!!!!!!!!!!!" + e.getMessage());
				// e.printStackTrace();
			}
		}

		// } catch (Exception ex) {
		// results[0] = "fail";
		// results[1] = "Error check Expected Result";
		// System.out.println("Error check Expected Result " + ex.getMessage());
		// // ex.getMessage();
		// }
		try {
			String rere = results[0];
			if (rere.equalsIgnoreCase("Fail")) {

				ExtentReport.captureScreenshot(driver, usecaseName, "ExpectedResult", TDID);
			}
		} catch (Exception ex) {
			results[0] = "fail";
			results[1] = "Error check Expected Result";
		}
		return results;
	}

	public static String text_click(WebDriver webDriver, String locator) {
		// Thread.sleep(1000);
		myWait(webDriver, locator);
		try {
			webDriver.findElement(By.xpath(locator)).click();
			check = "pass";
		} catch (Exception e) {
			check = "fail";
			error = "text_click";
			System.out.println(error + e.getMessage());
		}
		return check;
	}

	public String stopWatch(WebDriver webDriver) {
		try {
			StopWatch stopWatch = new StopWatch();

			WebDriverWait wait = new WebDriverWait(webDriver, 5000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Strong Room.7z']")));

			System.out.println("STARTING STOPWATCH");
			stopWatch.start();

			WebDriverWait waitstop = new WebDriverWait(webDriver, 5000);
			waitstop.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Done']")));
			System.out.println("STOPPING STOPWATCH");

			stopWatch.stop();
			System.out.println("Stopwatch time: " + stopWatch);

			// System.out.println("RESETTING STOPWATCH");
			// stopWatch.reset();
			// System.out.println("Stopwatch time: " + stopWatch);
			check = "pass";
		} catch (Exception e) {
			check = "fail";
			error = "text_click";
			System.out.println(error + e.getMessage());
		}
		return check;
	}
	
	public static String loop_select(WebDriver webDriver, String locator) {

		myWait(webDriver, locator);
		try {
			Thread.sleep(1000);
			WebElement we = webDriver.findElement(By.xpath(locator));
			//Test option
			List<WebElement> options = new Select(we).getOptions(); 
		    for (WebElement option : options){
		      System.out.println("Option :: "+option.getText());
		      list_select(webDriver, locator, option.getText());
		    }

		} catch (Exception ex) {
			check = "fail";
			error = "list_select";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	public static String download_report(WebDriver webDriver) {

		
		String linkFile = "";
//		final String dirs = System.getProperty("user.dir");
//		myWait(webDriver, locator);
		try {
			Thread.sleep(5000);
			Set<String> windows = webDriver.getWindowHandles();
			
			String xx = null;
			for (String string : windows) {
				xx = string;
				webDriver.switchTo().window(xx);
				if(webDriver.getCurrentUrl().contains("///C:/")){
					linkFile = webDriver.getCurrentUrl();
					System.out.println("path ::: " + linkFile);
				}
			}
			
//			 = "file:///C:/Users/Admin-PC/Downloads/ReportC1_20181123022924.pdf";
			
			String path = linkFile.substring(8);
			Thread.sleep(5000);
			Thread.sleep(5000);
			File file = new File(path);
			PDDocument document = PDDocument.load(file);

			// Instantiate PDFTextStripper class
			PDFTextStripper pdfStripper = new PDFTextStripper();

			// Retrieving text from PDF document
			String text = pdfStripper.getText(document);
			
			if (text == null) {
	            System.out.println("PDF to Text Conversion failed.");
	            check = "fail";
				error = "Invalid DownloadPDF";
	        }
	        else {
	            System.out.println("\nThe text parsed from the PDF Document....\n" + text);
	            check = "pass";
	        }
			
//			String chx = addIndexOf(text, "���˹�ҷ��");
//			
//			if (!chx.isEmpty()){
//				System.out.println("12345........\n" + chx);
//				check = "pass";
//			} else {
//				System.out.println("Error......" + chx);
//				check = "fail";
//				error = "Invalid DownloadPDF";
//			}
//			System.out.println(text);

			// Closing the document
			document.close();
			

		} catch (Exception ex) {
			check = "fail";
			error = "DownloadPDF";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}
	
	public static String url_open(WebDriver webDriver, String locator) {

		try {
			
			Thread.sleep(5000);
			webDriver.navigate().to(locator);
			
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "url_open";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}
	
	public static String event(String locator) {

		try {
			
			Thread.sleep(3000);
			Robot robot = new Robot();

			int num = Integer.parseInt(locator);
			// press s key to save
			System.out.println("press s key to save ");
			robot.setAutoDelay(2500);

			for (int i = 0; i < num; i++) {
				robot.keyPress(KeyEvent.VK_TAB);
			}
			
			robot.keyPress(KeyEvent.VK_ENTER);
			
			check = "pass";
		} catch (Exception ex) {
			check = "fail";
			error = "url_open";
			System.out.println(error + ex.getMessage());
		}

		return check;
	}

	
	public static String getCheck() {
		return check;
	}

	public static void setCheck(String check) {
		ManageKeyword.check = check;
	}

	public static String getError() {
		return error;
	}

	public static void setError(String error) {
		ManageKeyword.error = error;
	}
	
	public static String addIndexOf(String tx, String text) {
		int index = 0;
		String textHF = "";
		index = tx.indexOf(text);
		// System.out.println(index + " index");
		if (index > 0) {
			System.out.println(tx + " <><<<<<<<<<<<");
			textHF = tx;
		}
		return textHF;
	}
}

package com.gable.automated.testing.report;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.gable.automated.testing.constant.ManageConstant;


public class ExtentReport {
	static ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	static ExtentTest logger;

	@BeforeTest
	public static void startReport() {

		htmlReporter = new ExtentHtmlReporter(com.gable.automated.testing.constant.ManageConstant.REPORT_LOCATIONFILE);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", "SoftwareTestingMaterial");
		extent.setSystemInfo("Environment", "Automation Testing");
		extent.setSystemInfo("User Name", "G-able Automated Test Team");

		htmlReporter.config().setDocumentTitle("Extent Report");
		htmlReporter.config().setReportName("Extent Report ver 0.1");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.STANDARD);
	}

	@Test
	public static void createForTestData(String teseData, String tdID) {
		logger = extent.createTest(teseData + "_" + tdID);
	}

	@Test
	public static void stepPass(String step) {
		logger.log(Status.PASS, MarkupHelper.createLabel("TestStep : " + step + " : Pass", ExtentColor.GREEN));
	}

	@Test
	public static void stepFail(WebDriver mD, String useCaseName, String step, String TCID) throws Exception {
		captureScreenshot(mD, useCaseName, step, TCID);

	}

	@Test
	public static void stepSkip(String step) {

		logger.log(Status.SKIP, MarkupHelper.createLabel("TestStep : " + step + " : skip", ExtentColor.BLUE));
	}

	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {

			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
		}
	}

	@AfterTest
	public static void endReport() {
		extent.flush();
	}

	public static void captureScreenshot(WebDriver mD, String useCaseName, String step, String TCID)
			throws IOException {

		// Capture Screenshot
		TakesScreenshot ts = (TakesScreenshot) mD;

		File source = ts.getScreenshotAs(OutputType.FILE);

		String pathImg = ManageConstant.PATHIMAGE + "/" + useCaseName + "_" + step + "_" + TCID + ".png";

		FileUtils.copyFile(source, new File(pathImg));

		ExtentTest images = logger.addScreenCaptureFromPath(pathImg);

		logger.log(Status.FAIL, MarkupHelper.createLabel("TestStep : " + step + " : Fail", ExtentColor.RED));

	}

}

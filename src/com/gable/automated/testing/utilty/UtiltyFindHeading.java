package com.gable.automated.testing.utilty;

import com.gable.automated.testing.constant.ManageConstant;

public class UtiltyFindHeading {

	public int findHeadingToColumnNumber(String[][] sheet, String headingName) {
		boolean finding = false;
		int indexColumn = 0;

		do {

			if (sheet[ManageConstant.ROW_HEADING][indexColumn].equalsIgnoreCase(headingName)) {
				finding = true;
				return indexColumn;
			}
			indexColumn++;

		} while (!finding);
		return indexColumn;
	}
}

package com.gable.automated.testing.script;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;

import com.gable.automated.testing.constant.ManageConstant;

public class RunScript {
//	private static String driver = "T:";
	static Scanner scan = new Scanner(System.in);
	static List<String> fileList = new ArrayList<>();
	static String driveFile = "";

	public static void main(String[] args) {

//		String[] file = { "GHB_Automated_Test_CT-F03_1",
				/*"GHB_Automated_Test_CT-F03",
				"GHB_Automated_Test_CT-F04",
				"GHB_Automated_Test_CT-F07",
				"GHB_Automated_Test_CT-F14",
				"GHB_Automated_Test_CT-F15",
				"GHB_Automated_Test_CT-F16",
				"GHB_Automated_Test_CT-F17",
				"Funding_Automated_Test_OpenAccount",*/
//				"GHB_Automated_Test_CT-F22",
//				"GHB_Automated_Test_CT-F25"
//		};

		driverPlaylist();
		List<String> files = selectFile(fileList);
//		System.out.println(file.length + " <<<<");

		for (String name : files) {
			try {
				System.out.println("Waiting....");
//				 new Constant(args[0],args[1],args[2]);
				new ManageConstant(driveFile + "/AutomatedTester/Tools/BrowserDriver", driveFile + "/AutomatedTester/Data/Case/SRM/Admin_SRM/" + name, driveFile + "/AutomatedTester/Data/Results");
//				new ManageConstant( "T:/AutomatedTester/Tools/BrowserDriver", "T:/AutomatedTester/Data/Case/SRM/Admin_SRM/" + name + ".xlsx","T:/AutomatedTester/Data/Results");
				System.out.println("Waiting....");
			} catch (Exception e) {

				System.out.println("args[0] : " + "Path Webdiver");
				System.out.println("args[1] : " + "Path Read Excel");
				System.out.println("args[2] : " + "Path Result");
				e.printStackTrace();

			}

			try {
				TestCase tc = new TestCase();
				tc.readExecuteTestCaseYes();
				killProcesses();
			} catch (Exception e) {
				System.out.println("ERROR : " + e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public static void killProcesses() {

		try {
			String[] processNames = { /* "firefox",*/"iexplore", "chromedriver" };
			for (String process : processNames) {
				if (SystemUtils.IS_OS_LINUX) {
					String[] cmd = { "/bin/sh", "-c", "ps -ef | grep -w " + process
							+ " | grep -v grep | awk '/[0-9]/{print $2}' | xargs kill -9 " };

					StringBuffer output = new StringBuffer();
					try {
						Process p = Runtime.getRuntime().exec(cmd);
						List<String> result = IOUtils.readLines(p.getInputStream());
						for (String line : result) {
							System.out.println(line);
							output.append(line);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else if (SystemUtils.IS_OS_WINDOWS) {
					Runtime.getRuntime().exec("taskkill /F /IM " + process + ".exe");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static List<String> selectFile(List<String>/*String[]*/ file) {
//		System.out.println("Size file all :: " + file.size());
		List<String> list = new ArrayList<String>();
		

//		System.out.print("All File : 1\n" + "All HappyFlow : 2\n" + "All Validate : 3\n" + "All Admin HappyFlow : 4\n"
//				+ "All Admin Validate : 5\n" + "All Deposit HappyFlow : 6\n" + "All Deposit Validte : 7\n"
//				+ "Add File List : 8\n" + "Enter Number Here ::");
//		System.out.print("All File : 1\nAdd File List : 8\nEnter Number Here ::");
//		try {
//		System.out.println("-------------------------------------------------------------------------");
//		System.out.print("All File : 1 \n"
//				+ "Add File List : 2\n"
//				+ "Enter Number Here ::");
//		int selectNum = scan.nextInt();
		int selectNum = 1;

		
		switch (selectNum) {
		case 1:
//			String[] dpHappyFlow = {"Login(HappyFlow)", "DP(HappyFlow)","BR", "RE","Withdraw(HappyFlow)", "73", "45"};
			for (String all : file) {
				list.add(all);
				System.out.println(all);
			}
			System.out.println("Amount : " + list.size());
			break;
//		case 2:
//			String ans = "";
//
//			do {
//				String texts = "";
//				System.out.print("Enter file name :");
//				texts = scan.next();
//				String fileName = "";
//				for (String txs : file) {
//					fileName = addIndexOf(txs, texts);
//					if (!fileName.isEmpty()) {
//						list.add(fileName);
//					}
//				}
//
//				System.out.print("Do you want add file? (Y/N)");
//				ans = scan.next();
//			} while (ans.equalsIgnoreCase("Y")); //if Y break.
//			break;
		
		}
//}catch(Exception ex){
//			
//		}

		// System.out.print("Enter file name :");
		// String text = scan.nextLine();
		// for(String tx : file){
		// int index = 0;
		// index = tx.indexOf(text);
		//// System.out.println(index + " index");
		// if(index > 0){
		// System.out.println(tx + " <><<<<<<<<<<<");
		// list.add(tx);
		//
		// }
		// }
		//
		//

		return list;
	}

	public static String addIndexOf(String tx, String text) {
		int index = 0;
		String textHF = "";
		index = tx.indexOf(text);
		// System.out.println(index + " index");
		if (index > 0) {
			System.out.println(tx + " <><<<<<<<<<<<");
			textHF = tx;
		}
		return textHF;
	}
	
	public static void driverPlaylist(){
//		final String dirs = System.getProperty("user.dir");
		final String dirs = "T:";
		System.out.println("current dir = "+ dirs + "\\AutomatedTester\\Data\\Case\\SRM\\Admin_SRM\\");
		System.out.println("current dir = " + dirs );
		File folder = new File(dirs + "\\AutomatedTester\\Data\\Case\\SRM\\Admin_SRM\\");
//		driveFile = "T:";
		driveFile = dirs;
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {			
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());
				fileList.add(listOfFiles[i].getName());
			}
		}
	}

}

package com.gable.automated.testing.script;

import com.gable.automated.testing.constant.ManageConstant;
import com.gable.automated.testing.report.ExtentReport;
import com.gable.automated.testing.script.TestCase;
import com.gable.automated.testing.script.TestData;
import com.gable.automated.testing.script.TestStep;
import com.gale.automated.testing.excel.ManageExcel;

public class TestCase {

	static TestData testData = null;
	static String[][] testCase = null;
	static int maxlengthData = 0;
	private ManageExcel excels = new ManageExcel();
	private boolean check = false ;

	public TestCase() {
		testCase = ManageConstant.READTESTCASE;
		maxlengthDataYes();
	}

	public void readExecuteTestCaseYes() {
		// Start Report 
		ExtentReport.startReport();
		try {
			String useCaseName = "";
			String useCaseID = "";
			String level = "";
			testData = new TestData();
			
			for (int i = 1; i < testCase.length; i++) {
				String result = "";
				if (testCase[i][ManageConstant.INDEXCOLUMNEXCUTE_TESTCASE].equalsIgnoreCase("Y")) {
					useCaseName = testCase[i][ManageConstant.INDEXCOLUMNNAMEUSECASE_TESTCASE];
					useCaseID = testCase[i][ManageConstant.INDEXCOLUMNEXCUTE_TCID];
					level = testCase[i][ManageConstant.INDEXCOLUMNLEVEL_TESTCASE];
					
					System.out.println(useCaseID + "_" + useCaseName);
					
					result = testData.readExecuteTestDataYes(useCaseID, useCaseName, level, excels);					
					testCase[i][ManageConstant.INDEXCOLUMNRESULT_TESTCASE] = result;
					check = true;
				} else {
					testCase[i][ManageConstant.INDEXCOLUMNRESULT_TESTCASE] = "skip";
				}

			}

			if(check){
//				ManageExcel excel = new ManageExcel();
				excels.xlwrite_testCase(testCase);
				excels.tests_xlwrite_testStep(TestStep.testStepNew, (TestStep.testStepNew[0].length - 1));
				excels.finishWrite_TestResult();
//				excels.finishWrite_TestResult();
				System.out.println("Successful !!!  ");
			} else {
				System.out.println("�ѧ����ա�����͡ Execute TestCase �����  Y !!!!");
			}
		} catch (Exception ex) {

			
			System.out.println("Error " + ex.getMessage());
		}

		// End Report 
		ExtentReport.endReport();
	}

	public void maxlengthDataYes() {
		ManageExcel excels = new ManageExcel();
		String[][] data = null;
		String useCaseName = "";
		String useCaseID = "";
		int lengthData = 0;
		maxlengthData = 0;
		try {
			String path = ManageConstant.XLPATH_READ_TESTCASE;
			
			for (int i = 0; i < testCase.length; i++) {
				if (testCase[i][ManageConstant.INDEXCOLUMNEXCUTE_TESTCASE].equalsIgnoreCase("Y")) {
					useCaseName = testCase[i][ManageConstant.INDEXCOLUMNNAMEUSECASE_TESTCASE];
					useCaseID = testCase[i][ManageConstant.INDEXCOLUMNEXCUTE_TCID];
//					System.out.println(useCaseID + "_" + useCaseName);
					data = excels.xlRead(path, useCaseName);
					lengthData = data.length;
					if (maxlengthData < lengthData) {
						maxlengthData = lengthData;
					}
				}
			}
			
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
//		return (maxlengthData - 1);
	}

	
	public void printTestStep(String [][] setp){
		for (int i = 0; i < setp.length; i++) {
			for (int j = 0; j < setp[i].length; j++) {
				System.out.print(setp[i][j] + "----");
			}
			System.out.println();
		}
	}
	public static String resultTestCase() {
		return " ";
	}

	public static TestData getTestdata() {
		return testData;
	}

	public static void setTestdata(TestData testdata) {
		TestCase.testData = testdata;
	}

	public static String[][] getTestCase() {
		return testCase;
	}

	public static void setTestCase(String[][] testCase) {
		TestCase.testCase = testCase;
	}
}

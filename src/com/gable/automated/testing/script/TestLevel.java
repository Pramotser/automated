package com.gable.automated.testing.script;

public class TestLevel {
	private String result;
	private String pass = "Pass";
	private String fail = "Fail";

	public TestLevel() {

	}

	public String calculatePriorityLevel(String level, int amountTestDataForUseCaseYes, int amountPass) {
		boolean high = level.equalsIgnoreCase("high");
		boolean medium = level.equalsIgnoreCase("medium");
		boolean low = level.equalsIgnoreCase("low");

		System.out.println(" level " + level);
		System.out.println(" amountTestDataForUseCaseYes " + amountTestDataForUseCaseYes);
		System.out.println(" amountPass " + amountPass);
		if (high) {
			if (amountPass == amountTestDataForUseCaseYes)
				testPass();
			else
				testFail();
		} else if (medium || low) {
			if (amountTestDataForUseCaseYes >= 1 || amountTestDataForUseCaseYes <= 10) {
				if (amountPass == amountTestDataForUseCaseYes)
					testPass();
				else
					testFail();

			} else if (amountTestDataForUseCaseYes >= 11 || amountTestDataForUseCaseYes <= 20) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 1)))

					testPass();

				else if (low && (amountPass >= (amountTestDataForUseCaseYes - 2))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 21 || amountTestDataForUseCaseYes <= 30) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 2))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 3))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 31 || amountTestDataForUseCaseYes <= 40) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 3))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 4))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 41 || amountTestDataForUseCaseYes <= 50) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 4))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 5))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 51 || amountTestDataForUseCaseYes <= 60) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 5))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 6))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 61 || amountTestDataForUseCaseYes <= 70) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 6))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 7))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 71 || amountTestDataForUseCaseYes <= 80) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 7))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 8))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 81 || amountTestDataForUseCaseYes <= 90) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 8))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 9))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes >= 91 || amountTestDataForUseCaseYes <= 100) {
				if (medium && (amountPass >= (amountTestDataForUseCaseYes - 9))) {

					testPass();

				} else if (low && (amountPass >= (amountTestDataForUseCaseYes - 10))) {

					testPass();

				} else {
					testFail();
				}

			} else if (amountTestDataForUseCaseYes > 100) {
				int x = 0;
				if (medium) {
					x = (amountTestDataForUseCaseYes * 90) / 100;

				} else if (low) {
					x = (amountTestDataForUseCaseYes * 89) / 100;
				}

				if (amountPass >= x) {
					testPass();
				} else {
					testFail();
				}
			}

		}
		return result;

	}

	public void testPass() {
		result = pass;
	}

	public void testFail() {
		result = fail;
	}

}

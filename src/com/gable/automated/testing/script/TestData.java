package com.gable.automated.testing.script;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.gable.automated.testing.constant.ManageConstant;
import com.gable.automated.testing.report.ExtentReport;
import com.gable.automated.testing.utilty.UtiltyFindHeading;
import com.gale.automated.testing.excel.ManageExcel;

public class TestData {

	static TestStep testStep = null;
	static String[][] testData = null;
	static TestLevel testLevel = null;

	private static UtiltyFindHeading ufh = new UtiltyFindHeading();
	private WebDriver webdriver = null;
	private int indexColumnEXCUTE = 0;
	private int indexColumnURL = 0;
	private int indexColumnBROWSER = 0;
	private int indexColumnDIALOG = 0;
	private int indexColumnEXPECTED = 0;
	private int indexColumnRESULT = 0;
	private int indexColumnERROR = 0;
	private String sPath = null;
	private String[][] testStepNew = null;

	public TestData() {
		sPath = ManageConstant.XLPATH_READ_TESTCASE;
		testStep = new TestStep();
		createNewTestStep();
	}

	public String readExecuteTestDataYes(String TCID, String sheetName, String level, ManageExcel excels) {
		int amountTestDataForUseCaseYes = 0;
		int amountPass = 0;
		String resultTestData = "";
		String[] resultData = new String[2];
		try {
			testData = excels.xlRead(sPath, sheetName);
			testLevel = new TestLevel();

			indexColumnEXCUTE = ufh.findHeadingToColumnNumber(testData, "Execute");
			indexColumnURL = ufh.findHeadingToColumnNumber(testData, "URL");
			indexColumnBROWSER = ufh.findHeadingToColumnNumber(testData, "Browser");
			indexColumnDIALOG = ufh.findHeadingToColumnNumber(testData, "Dialog");
			indexColumnEXPECTED = ufh.findHeadingToColumnNumber(testData, "Expected Result");
			indexColumnRESULT = ufh.findHeadingToColumnNumber(testData, "Result");
			indexColumnERROR = ufh.findHeadingToColumnNumber(testData, "Error");
			for (int i = 1; i < testData.length; i++) {
				ExtentReport.createForTestData(sheetName, testData[i][0]);
				String[] dataIput = null;
				dataIput = testData[i];
				System.out.println(dataIput[0] + " indexColumnEXCUTE ::: " + dataIput[indexColumnEXCUTE]);
				if (dataIput[indexColumnEXCUTE].equalsIgnoreCase("Y")) {
					setWebDriver(testData[i][indexColumnBROWSER]);
				}
				resultData = testStep.readTestStep(TCID, sheetName, dataIput, testData[i][indexColumnEXCUTE],
						indexColumnURL, indexColumnBROWSER, indexColumnDIALOG, indexColumnEXPECTED, indexColumnRESULT,
						indexColumnERROR, testStepNew, (i - 1), webdriver);
				System.out.println(resultData[0] + " |__________________________result Test Data");

				if (testData[i][indexColumnEXCUTE].equalsIgnoreCase("Y")) {
					testData[i][indexColumnRESULT] = resultData[0];
					testData[i][indexColumnERROR] = resultData[1];
					amountTestDataForUseCaseYes++;

					if (resultData[0].equalsIgnoreCase("pass")) {
						amountPass++;
					}
				} else {
					testData[i][indexColumnRESULT] = "skip";
				}
				System.out.println("Result Test Data:: " + resultData[0]);
			}

			setTestStepNew(testStep.getTestStepNew());
			excels.xlwrite_testData(testData, sheetName);

			resultTestData = testLevel.calculatePriorityLevel(level, amountTestDataForUseCaseYes, amountPass);

			System.out.println("Finish!!! TestCase : " + sheetName);
		} catch (Exception e) {
			resultTestData = "fail";
			// closeWebDriver();
			System.out.println("ERROR!!! TestData Of TestCase :" + sheetName);
			// e.printStackTrace();
		}

		return resultTestData;
	}

	public void setWebDriver(String browserDri) {

		String browser = "";

		try {

			if (browserDri.equalsIgnoreCase("chrome.exe")) {
				browser = "chrome.exe";
				System.setProperty("webdriver.chrome.driver", ManageConstant.PATH_WEBDRIVER + "/chromedriver.exe");
				webdriver = new ChromeDriver();

			} else if (browserDri.equalsIgnoreCase("firefox")) {
				browser = "firefox";
				System.setProperty("webdriver.gecko.driver", ManageConstant.PATH_WEBDRIVER + "/geckodriver.exe");
				webdriver = new FirefoxDriver();

			} else if (browserDri.equalsIgnoreCase("IEDriverServer.exe")) {
				// Tomorrow !!!
				browser = "IEDriverServer.exe";
				System.out.println("123!!!!!!!!!!");

				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability("requireWindowFocus", true);
				System.setProperty("webdriver.ie.driver", ManageConstant.PATH_WEBDRIVER + "/IEDriverServer.exe");
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				webdriver = new InternetExplorerDriver(capabilities);

				// webdriver.manage().window().maximize();
				// DesiredCapabilities capabilities =
				// DesiredCapabilities.internetExplorer();
				// capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS,
				// true);
				// capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,
				// "http://excoss.localdomain:7777/sta-oss-e/service/index.xhtml");
				// System.setProperty("webdriver.ie.driver",
				// ManageConstant.PATH_WEBDRIVER + "/IEDriverServer.exe");
				// webdriver = new InternetExplorerDriver(capabilities);
				// checkBrowser = false ;

				System.out.println("goto set url");

			}

		} catch (Exception ex) {
			System.out.println(" ERROR web Driver" + browser );
			System.out.println(ex.getMessage());
		}
		webdriver.manage().window().maximize();
		webdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	public void createNewTestStep() {
		getTestStep();
		String[][] ts = TestStep.getTestStep();
		int colStep = ManageConstant.INDEXCOLUMNRESULT_TESTSTEP;
		int maxData = TestCase.maxlengthData;
		int maxColumn = colStep + (maxData - 1);
		int row = ts.length;
		testStepNew = new String[row][maxColumn];
		for (int i = 0; i < ts.length; i++) {
			int result = 1;
			for (int j = 0; j < maxColumn; j++) {
				if (colStep <= j) {

					if (i == 0) {
						testStepNew[i][j] = "Result_" + "TD00" + result;
						// System.out.print("|_|_|_|" + testStepNew[i][j]);
						result++;
					} else {
						testStepNew[i][j] = "-";
					}
				} else {
					testStepNew[i][j] = ts[i][j];
					// System.out.print(testStepNew[i][j] + "___");
				}

			}
		}

	}

	public static TestStep getTestStep() {
		return testStep;
	}

	public static void setTestStep(TestStep testStep) {
		TestData.testStep = testStep;
	}

	public static String[][] getTestData() {
		return testData;
	}

	public static void setTestData(String[][] testData) {
		TestData.testData = testData;
	}

	public String[][] getTestStepNew() {
		return testStepNew;
	}

	public void setTestStepNew(String[][] testStepNew) {
		this.testStepNew = testStepNew;
	}

}

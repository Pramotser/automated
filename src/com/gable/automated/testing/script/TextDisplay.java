package com.gable.automated.testing.script;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TextDisplay {

	private JFrame frame;
	private JTextField textEnter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TextDisplay window = new TextDisplay();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TextDisplay() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 651, 384);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		JLabel lblNewLabel = new JLabel("\u0E01\u0E23\u0E2D\u0E01\u0E14\u0E34!!!!!!");
		panel_1.add(lblNewLabel);
		
		textEnter = new JTextField();
		
		panel_1.add(textEnter);
		textEnter.setColumns(30);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel label = new JLabel("++\u0E41\u0E2A\u0E14\u0E07\u0E1C\u0E25\u0E15\u0E23\u0E07\u0E19\u0E35\u0E49\u0E40\u0E27\u0E49\u0E22++!!!!!!!");
		panel_2.add(label);
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);
		
		JLabel display = new JLabel("");
		textEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				display.setText(textEnter.getText());
			}
		});
		panel_3.add(display);
		
		JPanel panel_7 = new JPanel();
		panel_2.add(panel_7);
		
		JPanel panel_6 = new JPanel();
		panel_2.add(panel_6);
		
		JPanel panel_5 = new JPanel();
		panel_2.add(panel_5);
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
	}

}

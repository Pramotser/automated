package com.gable.automated.testing.script;

import java.util.Vector;

import org.openqa.selenium.WebDriver;

import com.gable.automated.testing.constant.ManageConstant;
import com.gable.automated.testing.report.ExtentReport;
import com.gale.automated.testing.keyword.ManageKeyword;

public class TestStep {
	private ManageKeyword keywords = null;
	static String[][] testStep = null;
	static String[][] testStepNew = null;

	int colData = 0;

	public TestStep() {
		testStep = ManageConstant.READTESTSTEP;
	}

	public Vector<Integer> rangTestStep(String TSID, String UseCaseName) {
		Vector<Integer> rang = new Vector<Integer>();
		for (int i = 0; i < testStep.length; i++) {
			if (testStep[i][ManageConstant.INDEXCOLUMNTSID_TESTSTEP].equalsIgnoreCase(TSID)
					&& testStep[i][ManageConstant.INDEXCOLUMNTCDESC_TESTSTEP].equalsIgnoreCase(UseCaseName)) {
				rang.add(i);
			}
		}
		return rang;
	}

	// return String[][] new test step
	public String[] readTestStep(String TSID, String UseCaseName, String[] testData, String EXCUTE, int indexColumnURL,
			int indexColumnBROWSER, int indexColumnDIALOG, int indexColumnEXPECTED, int indexColumnRESULT,
			int indexColumnERROR, String[][] newTestStep, int colStepResult, WebDriver webdriver) {
		boolean checkFail = false;
		boolean checkSkip = false;

		String[] resultTestStep = new String[2];

		String[] resultTestData = new String[2];
		try {

			Vector<Integer> rang = new Vector<Integer>();
			rang = rangTestStep(TSID, UseCaseName);
			colData = 4;

			int startStep = rang.get(0);
			int endStep = (rang.get(rang.size() - 1) + 1);
			int columnResult = ManageConstant.INDEXCOLUMNRESULT_TESTSTEP;

			// System.out.println("startStep " + startStep + " endStep" +
			// endStep);
			System.out.println(EXCUTE + " <<<<<<<<");

			keywords = new ManageKeyword(webdriver);

			for (int i = startStep; i < endStep; i++) {

			
				// checkFail = "";
				if (testStep[i][ManageConstant.INDEXCOLUMNTSID_TESTSTEP].equalsIgnoreCase(TSID)
						&& testStep[i][ManageConstant.INDEXCOLUMNTCDESC_TESTSTEP].equalsIgnoreCase(UseCaseName)) {
					if (EXCUTE.equalsIgnoreCase("Y")) {

						String keyword = testStep[i][ManageConstant.INDEXCOLUMNKEYWORD_TESTSTEP];
						String xpath = testStep[i][ManageConstant.INDEXCOLUMNELEMNET_TESTSTEP];
						System.out.println("keyword : " + keyword);
						
						
						if (keyword.equalsIgnoreCase("browser_open")) {
							System.out.println(testData[indexColumnURL] + " browser : " + testData[indexColumnBROWSER]);

							keywords.keyword_executor(keyword, xpath, testData[indexColumnURL]);
							resultTestStep[0] = keywords.getCheck();
							resultTestStep[1] = keywords.getError();
						} else if (keyword.equalsIgnoreCase("browser_close")) {
							// get dialog N/Y to Check action?
							System.out.println("DIALOG DIALOG DIALOG DIALOG DIALOG :::::::: " + testData[indexColumnDIALOG]);
							// go to method browser_close on manageKeyword Class.
							resultTestData = keywords.checkExpectedResult(testData[indexColumnDIALOG], testData[indexColumnEXPECTED], UseCaseName, testData[0]);

							System.out.println("checkExpectedResult::::::: " + resultTestData[0] + " Error ::" + resultTestData[1]);
//							String rere = resultTestData[0];
//							if (rere.equalsIgnoreCase("Fail")) {
////								System.out.println(":::1 " + rere + " ::2 " + rere);
//								ExtentReport.captureScreenshot(webdriver, UseCaseName, "ExpectedResult", TSID);
////								System.out.println(":::2121 " + rere + " ::2323 " + rere);
//							} 
							keywords.keyword_executor(keyword, "", "");
							resultTestStep[0] = keywords.getCheck();
							resultTestStep[1] = keywords.getError();

							colData = 4;
						} else {
							if (keyword.equalsIgnoreCase("edit_input") || keyword.equalsIgnoreCase("edit_action")
									|| keyword.equalsIgnoreCase("upload_file")
									|| keyword.equalsIgnoreCase("radio_select") || keyword.equalsIgnoreCase("display")
									/*
									 * ||
									 * keyword.equalsIgnoreCase("dialog_click")
									 */
									|| keyword.equalsIgnoreCase("list_select")
									|| keyword.equalsIgnoreCase("checkbox_set")
									|| keyword.equalsIgnoreCase("remember")) {
								// get TestData Class [][]
								System.out.println("Enter data ::::: " + testData[colData]);

								keywords.keyword_executor(keyword, xpath, testData[colData]);
								resultTestStep[0] = keywords.getCheck();
								resultTestStep[1] = keywords.getError();
								colData++;
							} else {
							
								keywords.keyword_executor(keyword, xpath, "");
								resultTestStep[0] = keywords.getCheck();
								resultTestStep[1] = keywords.getError();
							}
						}
						
						System.out.println("Result keyword:: " + resultTestStep[0]);
						if (resultTestStep[0].equalsIgnoreCase("fail")) {
							checkFail = true;
							ExtentReport.stepFail(webdriver, UseCaseName, keyword, testData[0]);
							newTestStep[i][columnResult + colStepResult] = resultTestStep[0] + "_" + resultTestStep[1];
						} else {
							ExtentReport.stepPass(keyword);
							newTestStep[i][columnResult + colStepResult] = resultTestStep[0];
						}

					} else if (EXCUTE.equalsIgnoreCase("N")) {

						ExtentReport.stepSkip(testStep[i][ManageConstant.INDEXCOLUMNKEYWORD_TESTSTEP]);
						newTestStep[i][columnResult + colStepResult] = "skip";
						checkSkip = true;
					}
				}

				
			}

			if (checkFail) {
				resultTestData[0] = "fail";
				resultTestData[1] = "Test Step Fail";
			} else if (checkSkip) {
				resultTestData[0] = "skip";
				resultTestData[1] = "";
			}
		} catch (Exception ex) {
			resultTestData[0] = "fail";
			resultTestData[1] = "Error Process TestStep";
			System.out.println("Error Process TestStep:: " + ex);
		}
		setTestStepNew(newTestStep);
//		closeWebDriver(webdriver);
		return resultTestData;

	}

	public void closeWebDriver(WebDriver webdriver) {

		try {
			Thread.sleep(5000);
			webdriver.quit();
		} catch (InterruptedException e) {
			System.out.println("ERROR closeWebDriver" + e.getMessage());
			// e.printStackTrace();
		}

	}

//	public String[] checkExpectedResult(WebDriver webdriver, String dialog, String expecResult){
//
//		String results[] = new String[2];
//
//		String[] temp = dialog.split(",");
////		try {
//			if (temp[0].equalsIgnoreCase("Y")) {
//				try {
//					if (temp[1].equalsIgnoreCase("dialog_accept")) {
//						results[0] = keywords.dialog_accept(webdriver, expecResult);
//						results[1] = keywords.getError();
//					} else if (temp[1].equalsIgnoreCase("dialog_dismiss")) {
//						results[0] = keywords.dialog_dismiss(webdriver, expecResult);
//						results[1] = keywords.getError();
//					} else {
//						results[0] = keywords.errorMsg(webdriver, temp[1], expecResult);
//						results[1] = keywords.getError();
//					}
//				} catch (Exception e) {
//					results[0] = "Fail";
//					results[1] = "Invalid Dialog Expected Result";
//					System.out.println( " Fail Dialog ���++++++++++!!!!!!!!!!!!!");
//				}
//			} else if (temp[0].equalsIgnoreCase("N")) {
//				try {
////					Thread.sleep(3000);
//				
//				System.out.println("ExResult =  " + expecResult + " AcResult = " + webdriver.getTitle() + " !!!");
//				if (webdriver.getTitle().equals(expecResult)) {
//					results[0] = "Pass";
//					results[1] = "";
//					// results[1] = expecResult;
//				} else {
//					results[0] = "Fail";
//					results[1] = "ExResult:  " + expecResult + "UnEquals AcResult: " + webdriver.getTitle() + " !!!";
//				}
//				} catch (Exception e) {
//					results[0] = "Fail";
//					results[1] = "Invalid Title Expected Result";
//					System.out.println( " Fail Title ���++++++++++!!!!!!!!!!!!!" + e.getMessage());
////					e.printStackTrace();
//				}
//			}
//
////		} catch (Exception ex) {
////			results[0] = "fail";
////			results[1] = "Error check Expected Result";
////			System.out.println("Error check Expected Result " + ex.getMessage());
////			// ex.getMessage();
////		}
//		return results;
//	}

	public static String[][] getTestStep() {
		return testStep;
	}

	public static void setTestStep(String[][] testStep) {
		TestStep.testStep = testStep;
	}

	public static String[][] getTestStepNew() {
		return testStepNew;
	}

	public static void setTestStepNew(String[][] testStepNew) {
		TestStep.testStepNew = testStepNew;
	}

}

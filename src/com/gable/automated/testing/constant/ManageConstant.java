package com.gable.automated.testing.constant;

import com.gable.automated.testing.constant.ManageConstant;
import com.gable.automated.testing.utilty.UtiltyFindHeading;
import com.gale.automated.testing.excel.ManageExcel;

public class ManageConstant {

	public static String PATH_WEBDRIVER = null;
	public static String XLPATH_READ_TESTCASE = null;
	public static String XLPATH_WRITE_TESTRESULT_OUTPUT = null;
	public static String REPORT_LOCATIONFILE = null;
	public static String PATHIMAGE = null;

	private static UtiltyFindHeading ufh = new UtiltyFindHeading();
	public static ManageExcel EXCELS = new ManageExcel();
	public static final String SHEET_NAME_TESTCASE = "TestCases";
	public static final String SHEET_NAME_TESTSTEP = "TestSteps";
	public static String[][] READTESTCASE = null;
	public static String[][] READTESTSTEP = null;
	public static int ROW_HEADING = 0;
	public static int INDEXCOLUMNEXCUTE_TESTCASE;
	public static int INDEXCOLUMNEXCUTE_TCID;
	public static int INDEXCOLUMNNAMEUSECASE_TESTCASE;
	public static int INDEXCOLUMNLEVEL_TESTCASE;
	public static int INDEXCOLUMNRESULT_TESTCASE;
	
	public static int INDEXCOLUMNTSID_TESTSTEP;
	public static int INDEXCOLUMNTCDESC_TESTSTEP;
	public static int INDEXCOLUMNKEYWORD_TESTSTEP;
	public static int INDEXCOLUMNELEMNET_TESTSTEP;
	public static int INDEXCOLUMNERROR_TESTSTEP;
	public static int INDEXCOLUMNRESULT_TESTSTEP;
	
	public ManageConstant(String PATH_WEBDRIVER, String XLPATH_READ_TESTCASE, String XLPATH_WRITE_TESTRESULT_OUTPUT) {

		ManageConstant.PATH_WEBDRIVER = PATH_WEBDRIVER;
		ManageConstant.XLPATH_READ_TESTCASE = XLPATH_READ_TESTCASE;
		ManageConstant.XLPATH_WRITE_TESTRESULT_OUTPUT = XLPATH_WRITE_TESTRESULT_OUTPUT + nameOutput() + ".xlsx";
		ManageConstant.REPORT_LOCATIONFILE = XLPATH_WRITE_TESTRESULT_OUTPUT + nameOutput() + ".html";
//		ManageConstant.XLPATH_WRITE_TESTRESULT_OUTPUT = XLPATH_WRITE_TESTRESULT_OUTPUT + "\\TestResult_Output_NameProject.xlsx";
//		ManageConstant.REPORT_LOCATIONFILE = XLPATH_WRITE_TESTRESULT_OUTPUT + "\\ReportsNameProject.html";
		ManageConstant.PATHIMAGE = XLPATH_WRITE_TESTRESULT_OUTPUT + "\\Images_Test\\";
		ManageConstant.READTESTCASE = EXCELS.xlRead(ManageConstant.XLPATH_READ_TESTCASE, ManageConstant.SHEET_NAME_TESTCASE);
		ManageConstant.READTESTSTEP = EXCELS.xlRead(ManageConstant.XLPATH_READ_TESTCASE, ManageConstant.SHEET_NAME_TESTSTEP);
		// TEST CASE
		ManageConstant.INDEXCOLUMNEXCUTE_TESTCASE = ufh.findHeadingToColumnNumber(READTESTCASE, "Execute");
		ManageConstant.INDEXCOLUMNEXCUTE_TCID = ufh.findHeadingToColumnNumber(READTESTCASE, "TCID");
		ManageConstant.INDEXCOLUMNNAMEUSECASE_TESTCASE = ufh.findHeadingToColumnNumber(READTESTCASE, "Name");
		ManageConstant.INDEXCOLUMNRESULT_TESTCASE = ufh.findHeadingToColumnNumber(READTESTCASE, "Result");
		ManageConstant.INDEXCOLUMNLEVEL_TESTCASE = ufh.findHeadingToColumnNumber(READTESTCASE, "Level");
		// TEST STEP Keyword
		ManageConstant.INDEXCOLUMNTSID_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "TSID");
		ManageConstant.INDEXCOLUMNTCDESC_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "TC Desc");
		ManageConstant.INDEXCOLUMNKEYWORD_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "Keyword");
		ManageConstant.INDEXCOLUMNELEMNET_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "Xpath");
//		ManageConstant.INDEXCOLUMNERROR_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "Error");
		ManageConstant.INDEXCOLUMNRESULT_TESTSTEP = ufh.findHeadingToColumnNumber(READTESTSTEP, "Result");
		// TEST DATA ON TestDataClass
		

	}
	
	public String nameOutput(){
		String pathInput = ManageConstant.XLPATH_READ_TESTCASE;
		String[] sub = pathInput.split("/");
		String last = "";
		for (String s : sub){
			last = s;
		}
		
		String[] sName = last.split("\\.");
		return "\\TestResult_" + sName[0];
	}
}
